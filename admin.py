from django.contrib import admin
from .models import Link, LinkAudit

# Register your models here.
admin.site.register(Link)
admin.site.register(LinkAudit)