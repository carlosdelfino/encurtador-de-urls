from django.db import models

# Create your models here.
class Link(models.Model):
    destino = models.URLField(blank = False,
                           verbose_name = "Link a ser encurtado", 
                           help_text = "Digite o Link a ser encurtado, digite a url completa.")
    token = models.CharField(unique = True, max_length = 24,
                             verbose_name = 'Token', 
                             help_text = 'Digite o Token para URL encurtada.')
    
    count = models.PositiveIntegerField(default=0)
    
    def __str__(self) -> str:
        return self.token
    
class LinkAudit(models.Model):
    link = models.ForeignKey(
        Link,
        on_delete=models.DO_NOTHING,
        verbose_name='Link',
        help_text="Link encurtada"
    )
    ip_origem = models.GenericIPAddressField(verbose_name="IP de Origem",
                                      help_text="IP de origem da requisição.")
    data_hora_origem = models.DateTimeField(verbose_name="Data e Hora de origem",
                                       help_text="Data e hora de origem da requisição.")
    
    def __str__(self) -> str:
        if self.link:
            return self.link.__str__() + ' -> ' + self.ip_origem + ' - ' + self.data_hora_origem.__str__()
        else:
            return ' -> ' + self.ip_origem + ' - ' + self.data_hora_origem.__str__()