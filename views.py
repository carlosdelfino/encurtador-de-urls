from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from .forms import FormLinks
from .models import Link, LinkAudit
from django.utils import timezone

# Create your views here.
def home(request):
    form = FormLinks()
    return render(request, 'home.html', {'form': form})

def valida_link(request):
    form = FormLinks(request.POST)
    
    if form.is_valid():
        form.save()
        status = 99
    else:
        status = 1
    
    return render(request, 'home.html', {'form': form,
                                         'status': status})
    
def redirecionar_token(request, token):
    try:
        link = Link.objects.get(token=token)
        link.count = link.count + 1
        link.save()

        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        audit = LinkAudit()    
        audit.ip_origem = ip
        audit.data_hora_origem = timezone.now()        
        audit.link = link
        audit.save()

    except Link.DoesNotExist:
        link = None
        return render(request, 'inexistente.html',{'token': token})
    
    return redirect(link.destino, permanent=False)

def audit_token_home(request):
    links = Link.objects.all()
    return render(request, 'audittoken_home.html', {'links': links})
    
def audit_token(request, token):
    try:
        link = Link.objects.get(token=token)
        try:
            audits = LinkAudit.objects.filter(link_id=link.id)
        except LinkAudit.DoesNotExist as err:
            audits = None
        
        return render(request, 'audittoken.html',{'link': link, 'audits': audits})
    except Link.DoesNotExist:
        link = None
        return render(request, 'inexistente.html',{'token': token})    