from django.urls import path
from . import views

urlpatterns = [
    path('', 
         views.home,
         name = 'encurtador_home'
        ),
    path('audit',
         views.audit_token_home,
         name='audit_token_home'),
    path('audit/<str:token>',
         views.audit_token,
         name='audit_token'),
    path('valida_link', 
         views.valida_link,
         name="valida_link"),
    path('<str:token>',
         views.redirecionar_token, 
         name='redirecionar_token')
]